# Scraper challenge

## Setup

1. Clone this repo and make python>=3.4 virtualenv (not needed, but recommended).
2. `pip install -r requirements.txt`
3. Look at example spider `apps/scraper/spiders/tech_london.py`, run it by `scrapy crawl tech_london`.

## Test assignment

Write your own spider utilizing our base spider class `apps.scraper.base_spider.ProfileListSpider` for <https://masschallenge.org/startups>.

IMPORTANT - do not let it crawl all startups, so you won't overload their website. 
Just few of them to check it's getting what it suppose to is enough.
You can use parameter `-o data.csv` to store crawled data to CSV file.

Goal is to scrape as much of startup information as possible. List of all possible result dict keys is at the end of README.

When done, send your spider as attachment to [vojtech@leadspicker.com](mailto:vojtech@leadspicker.com) (don't commit it to repo!). 
Please do mention approx. how long it took you to write it.

Deadline is Monday 8.7.2019 at midnight. If you are on vacation during that time, just write me and I can extend it.

Please do not use any of code provided elsewhere, this is not opensource.

## All possible result dict keys

* `"name"` - startup name
* `"oneliner"` - short description, usually one sentence
* `"description"` - long description
* `"specialization"` - industries of startup, devided by pipe ( | )
* `"started"` - date when founded (ISO format, can be just year or year with month)
* `"url"` - URL of startup website
* `"size"` - number of employees (or interval, example: "2-10")
* `"state"` - country of startup headquarters
* `"city"` - city of startup headquarters (use this key if value contains country, city and other location info)
* `"logo"` - URL of startup logo
* `"twitter"` - URL of twitter profile 
* `"facebook"` - URL of facebook profile
* `"linkedin"` - URL of linkedin profile
* `"crunchbase"` - URL of crunchbase profile
* `"angellist"` - URL of angellist profile
* `"wechat"` - URL of wechat profile
* `"email"` - Email contact of company
* `"phone"` - Phone contact of company
* `"total_funding"` - Total funding ammount in $
* `"funding_rounds"` - List of dicts with keys: {name, is_exit (bool), date_raised (required), raised (required, in $)}
* `"is_live"` - Boolean flag if company is in business or dead
* `"detail"` - If using `ProfileListSpider` with no major changes, it is there automatically. Otherwise add it, this should be URL of startup profile on website being scraped. Must be unique.


