# -*- coding: utf-8 -*-
import pytest
from datetime import datetime
from libs import parsers
import libs.data


@pytest.mark.parametrize("test_input,expected", [
    ("", None),
    ("asdfg", None),
    ("2010", 2010),
    ("  1960  \n", 1960),
    ("Some year 2010", 2010),
    ("25.6.2010 ", 2010),
])
def test_parse_year(test_input, expected):
    assert parsers.parse_year(test_input) == expected


@pytest.mark.parametrize("test_input,expected", [
    ("", (None, None)),
    ("asdasf", (None, None)),
    ("5,21", (5, 21)),
    ("Num of employees 5 - 21", (5, 21)),
    (" 5 employees", (5, 5)),
    ("size 543 employees", (543, 543)),
])
def test_parse_min_max_employees(test_input, expected):
    assert parsers.parse_min_max_employees(test_input) == expected


@pytest.mark.parametrize("test_input,expected", [
    ("", None),
    ("asd", None),
    (2010, datetime(2010, 1, 1)),
    ("August 2009", datetime(2009, 8, 1)),
    (" 2010  \n", datetime(2010, 1, 1)),
    ("6/20/2010", datetime(2010, 6, 20)),
    ("20.6.2010", datetime(2010, 6, 20)),
])
def test_parse_date(test_input, expected):
    assert parsers.parse_date(test_input) == expected


@pytest.mark.parametrize("test_input,expected", [
    (123, 123),
    ('123', 123),
    ('$100000', 100000),
    ('$100,000', 100000),
    ('100.000$', 100000),
    ('123 eur', 149),
    ('€123M', 149526257),
    ("", None),
    ("asd", None),
    ("$1m+", 1000000),
    ("$500K - 1m", 750000),
    ("$5k", 5000),
    ("100000", 100000),
    ("$9.6M", 9600000),
    ("$22M", 22000000),
    ("$375,0K", 375000),
    ("$569.91K", 569910),
    ("$1.2B", 1200000000),
    ("$510,62M", 510620000),
])
def test_parse_funding(test_input, expected, monkeypatch):
    monkeypatch.setattr(libs.data, 'EXCHANGE_RATES', {
        'USDEUR': 0.822598,
    })
    assert parsers.parse_funding(test_input) == expected
