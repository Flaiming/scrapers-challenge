# -*- coding: utf-8 -*-
import scrapy

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html


class ScrapersPipeline(object):
    def process_item(self, item, spider):
        if 'detail' not in item:
            raise scrapy.exceptions.CloseSpider("There is no 'detail' attribute in result item data! Please add one.")
        return item
