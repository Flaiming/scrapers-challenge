from libs import parser


class By:
    CSS = 'css'
    CSS_LIST = 'css_list'
    XPATH = 'xpath'
    VALUE = 'value'
    META = 'meta'


class ResponseParser:

    def __init__(self, response):
        self.response = response

    def find_elements(self, method, selector, fce=None):
        if method == By.VALUE:
            res = selector
        else:
            res = [x.strip() if x is not None else x for x in getattr(self.response, method)(selector).extract()]
        if fce:
            return [fce(self.response, r) for r in res]
        return res

    def find_element(self, method, selector, fce=None):
        if method == By.VALUE:
            res = selector
        elif method == By.META:
            res = self.response.meta.get(selector, "")
        elif method == By.CSS_LIST:
            res = ' '.join(self.response.css(selector).extract())
        else:
            res = getattr(self.response, method)(selector).extract_first()
        if res:
            res = res.strip()
        if fce and res:
            return fce(self.response, res)
        return res


def make_full_url(response, url):
    if url.startswith('/'):
        return "/".join(response.url.split('/')[:3]) + url
    if url.startswith('?') or url.startswith('./'):
        return "/".join(response.url.split('/')[:-1]) + url
    if not url.startswith("http"):
        return "/".join(response.url.split('/')[:-1]) + "/" + url
    return url


def parse_year(response, text):
    return parser.parse_year(text)


def parse_date(response, text):
    return parser.parse_date(text)


def parse_funding(response, text):
    return parser.parse_funding(text)


