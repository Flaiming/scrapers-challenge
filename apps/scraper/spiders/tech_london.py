# -*- coding: utf-8 -*-
from apps.scraper.base_spiders import ProfileListSpider
from apps.scraper.parser import By, parse_year, make_full_url


def parse_description(response, text):
    return "\n\n".join(response.css('.story-content p::text').extract())


class TechLondonSpider(ProfileListSpider):
    name = "tech_london"
    start_urls = ["https://www.tech.london/startups"]
    next_page_template = "https://www.tech.london/startups?page={page}"

    source_name = 'tech.london'
    list_selector = (By.CSS, ".startups a.dark::attr(href)")
    profile_selectors = {
        'name': (By.CSS, '.profile-header h1.black::text'),
        'oneliner': (By.CSS, '.profile-header .black+ p::text'),
        'description': (By.CSS, 'body', parse_description),
        'started': (By.CSS, '.time::text', parse_year),
        'url': (By.CSS, 'a.lined::attr(href)'),
        'twitter': (By.XPATH, '//span[@class="icon-twitter"]/parent::a/@href'),
        'facebook': (By.XPATH, '//span[@class="icon-facebook"]/parent::a/@href'),
        'linkedin': (By.XPATH, '//span[@class="icon-linkedin"]/parent::a/@href'),
        'logo': (By.CSS, '.panel img::attr(src)', make_full_url),
        'size': (By.XPATH, 'string(//p[@class="byline semi zeta"]/span[contains(text(),"Employees")]/following-sibling::span)'),
        'state': (By.VALUE, 'United Kingdom'),
        'city': (By.VALUE, 'London'),
    }
    custom_settings = {
    }

