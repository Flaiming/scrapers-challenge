# -*- coding: utf-8 -*-
from apps.scraper.base_spiders import ProfileListSpider
from apps.scraper.parser import By, parse_year


def split_mailto(_response, text):
    return text.split(":")[1]


def parse_specializations(response, _text):
    return " | ".join(response.css('.field-items *::text').getall())


def parse_city(_response, text):
    if not text:
        return ""
    state = text.lower()
    to_be_replaced = ["diamond", "platinum", "gold", "silver", "fintech", "finalist", "healthtech", "finalist"]
    for i in to_be_replaced:
        state = state.replace(i, "")
    return "".join([i for i in state if not i.isdigit()])


class MasschallengeSpider(ProfileListSpider):
    name = 'masschallenge_org'
    source_name = 'masschallenge_org'
    start_urls = ['https://masschallenge.org/startups']
    next_page_template = "https://masschallenge.org/startups?page={page}"

    list_selector = (By.XPATH, "//div[@class='card startup']//@href")

    profile_selectors = {
        'name': (By.CSS, ".card-title::text"),
        'url': (By.XPATH, "//div[@class='startup-links']/a[img[contains(@src, 'mc-globe')]]/@href"),
        'oneliner': (By.CSS, ".startup-tweetablepitch::text"),
        'description': (By.CSS, ".startup-fullelevatorpitch::text"),
        'specialization': (By.CSS, ".field-items", parse_specializations),
        'logo': (By.XPATH, "//div[@class='startup-detail-logo']/img[contains(@src, 'logo')]/@src"),
        'twitter': (By.XPATH, "//div[@class='startup-links']/a[contains(@href, 'twitter.com')]/@href"),
        'facebook': (By.XPATH, "//div[@class='startup-links']/a[contains(@href, 'facebook.com')]/@href"),
        'linkedin': (By.XPATH, "//div[@class='startup-links']/a[contains(@href, 'linkedin.com')]/@href"),
        'email': (By.XPATH, "//div[@class='startup-links']/a[contains(@href, 'mailto')]/@href", split_mailto),
        'city': (By.CSS, ".winners-class::text", parse_city),
        'started': (By.CSS, '.card-body p.lead::text', parse_year),
    }

    custom_settings = {
    }
